var name = "";
var uid = "";
var iconKey = "";
var stream = "global";

$(function() {
	$("#login-form").submit(function(){
		$.ajax({
			type: "POST",
			url: "./login",
			data: {"user":$("#login-input-user").val()},
			dataType: "json",
			success: function(data){
				uid = data.uid;
				name = data.name;
				iconKey = data.icon;
				$("#login-form").hide(1000);
				LetterPost.initialize();
				LetterList.initialize();
				ChannelListener.initialize();
			}
		})
	})
})

var LetterPost = {
	initialize: function(){
		$("<form />")
		 .addClass("well")
		 .attr("id","post-form")
		 .attr("action","javascript:void(0)")
		 .append(
				 $("<div />")
				  .attr("id","post-input-icon")
		 )
		 .append(
				 $("<input />")
				  .addClass("input-medium")
				  .attr("type","text")
				  .attr("id","post-input-name")
				  .val(name)
		 )
		 .append(
				 $("<span />")
				  .attr("id","post-input-uid")
				  .text("CODE: #"+uid)
		 )
		 .append($("<br />"))
		 .append(
				 $("<textarea  />")
				  .attr("id","post-input-text")
		 )
		 .append($("<br />"))
		 .append(
				 $("<input	/>")
				  .addClass("btn")
				  .attr("type","submit")
				  .val("Post")
		 )
		 .submit(this.doPost)
		 .hide()
		 .appendTo("#letter-post")
		 .show(1000);
	},

	doPost: function(){
		$.ajax({
			type: "POST",
			url: "./post",
			data: {
				"uid":uid,
				"name":$("#post-input-name").val(),
				"icon":iconKey,
				"data":$("#post-input-text").val()
				},
			dataType: "json",
			success: function(){
				name = $("#post-input-name").val();
				$("#post-input-text").val("");
			}
		})
	}
};

var LetterList = {
	initialize: function(){
		var self = this;
		$.ajax({
			type: "GET",
			url: "./list",
			data: {
				"num" : 30,
				"stream" : stream
			},
			dataType: "json",
			success: function(data) {
				if (!data[0]) return;
				$("#letter-list").hide();
				data.forEach(function(e) {
					self.getLetterObject(e).appendTo("#letter-list");
                });
				$("#letter-list").show(1000);
			}
		});
	},
	
	appendLetter: function(letter){
		this.getLetterObject(letter)
		 .hide()
		 .prependTo("#letter-list")
		 .show(1000);
	},

	getLetterObject: function(letter){
		return $("<div />")
		 .addClass("well")
		 .addClass("letter")
		 .append(
				 $("<span />")
				  .addClass("letter-name")
				  .text(letter.name)
		 )
		 .append(
				 $("<span />")
				  .addClass("letter-uid")
				  .text("id:"+letter.uid.substring(0,8))
		 )
		 .append(
				 $("<span />")
				  .addClass("letter-time")
				  .text(letter.date)
		 )
		 .append(
				 $("<p />")
				  .addClass("letter-text")
				  .text(letter.data)
		 )
	}
}

var ChannelListener = {
	initialize: function(){
		this.channelOpen();
	},

	channelOpen: function(){
		self = this;
		$.ajax({
			type: "GET",
			url: "./token",
			data: {
				"uid" : uid,
				"stream" : stream
			},
			success: function(data) {
				channel = new goog.appengine.Channel(data);
				socket = channel.open();
				socket.onmessage = self.applyMessage;
				socket.onclose   = self.channelOpen;
			}
		});
	},

	applyMessage: function(msg){
		data = $.parseJSON(msg.data);
		if(data.stream == stream){
			LetterList.appendLetter(data);
		}
	}
}