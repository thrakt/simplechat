package schat.service;

import java.util.Date;
import java.util.List;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;

import schat.dao.ChannelIdModelDao;
import schat.model.ChannelIdModel;
import schat.model.LetterModel;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static schat.CommonValue.*;

public class NotificationServiceTest extends AppEngineTestCase {

    private NotificationService service = new NotificationService();

    @Test
    public void test() throws Exception {
        assertThat(service, is(notNullValue()));
    }

    @Test
    public void createToken(){
        String uid = "setUid";
        String token = NotificationService.createToken(uid);

        assertThat(token, is(notNullValue()));

        ChannelIdModel model = new ChannelIdModelDao().getByChannelId(UserService.fixUserId(uid));

        assertThat(model, is(notNullValue()));
    }

    @Test
    public void createTokenTwice(){
        String uid = "setUid";
        String token = NotificationService.createToken(uid);
        String token2 = NotificationService.createToken(uid);

        assertThat(token, is(notNullValue()));
        assertThat(token2, is(notNullValue()));
        assertThat(token2, is(not(token)));

        List<ChannelIdModel> list = new ChannelIdModelDao().getByStream(DEFAULT_STREAM);

        assertThat(list.size(), is(equalTo(1)));
    }

    @Test
    public void notifyPost(){
        ChannelIdModel idModel = new ChannelIdModel();
        String setcid = "setChannelId";
        idModel.setChannelId(setcid);
        idModel.setStream(DEFAULT_STREAM);
        idModel.setCreateTime(new Date().getTime()-7500001);

        LetterModel model = new LetterModel();
        model.setPostName("setName");
        model.setPostDate(new Date());
        model.setUserId("setFixUid");
        model.setWriteData("setText");
        model.setStream(DEFAULT_STREAM);

        NotificationService.notifyPost(model);

        assertThat(new ChannelIdModelDao().getByChannelId(setcid), is(nullValue()));

    }
}
