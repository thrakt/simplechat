package schat.service;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;

import schat.dao.UserModelDao;
import schat.model.UserModel;
import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.*;
import static org.hamcrest.CoreMatchers.*;
import static schat.CommonValue.*;

public class UserServiceTest extends AppEngineTestCase {

    private UserService service = new UserService();

    @Test
    public void test() throws Exception {
        assertThat(service, is(notNullValue()));
    }

    @Test
    public void getUserById(){
        UserModel model = new UserModel();
        String userId = "setUid";
        model.setName(DEFAULT_USER_NAME);
        model.setUserId(UserService.fixUserId(userId));
        new UserModelDao().put(model);

        UserModel userById = UserService.getUserById(UserService.fixUserId(userId));

        assertThat(userById, is(equalTo(model)));
    }

    @Test
    public void fixUserId(){
        String rawId = "rawUserId";
        String fixId = UserService.fixUserId(rawId);

        assertThat(fixId, is(not(containsString(rawId))));
    }

    @Test
    public void createUser(){
        String name = "setName";
        String id = "setUid";
        UserModel model = UserService.createUser(name, id);

        assertThat(model.getName(), is(equalTo(name)));
        assertThat(model.getUserId(), is(equalTo(UserService.fixUserId(id))));
    }

    @Test
    public void createUserWithUserId(){
        String userId = "setUid";
        UserModel model = UserService.createUserWithUserId(userId);

        assertThat(model.getName(), is(equalTo(DEFAULT_USER_NAME)));
        assertThat(model.getUserId(), is(equalTo(UserService.fixUserId(userId))));
    }

    @Test
    public void allocateUserId() throws InterruptedException{
        String id1 = UserService.allocateUserId();
        String id2 = UserService.allocateUserId();
        String id3 = UserService.allocateUserId();

        assertThat(id1, not(equalTo(id2)));
        assertThat(id1, not(equalTo(id3)));
        assertThat(id2, not(equalTo(id3)));
    }

    @Test
    public void updateUserName(){
        UserModel model = new UserModel();
        String userId = "setUid";
        model.setName(DEFAULT_USER_NAME);
        model.setUserId(UserService.fixUserId(userId));
        UserModelDao dao = new UserModelDao();
        dao.put(model);

        String updName = "updateName";
        UserService.updateName(userId, updName);

        UserModel model2 = dao.getByUserId(UserService.fixUserId(userId));

        assertThat(model2.getName(), is(equalTo(updName)));
    }
}
