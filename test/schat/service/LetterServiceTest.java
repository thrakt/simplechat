package schat.service;

import java.util.List;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;

import schat.dao.LetterModelDao;
import schat.dao.UserModelDao;
import schat.model.LetterModel;
import schat.model.UserModel;
import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.*;
import static org.hamcrest.CoreMatchers.*;
import static schat.CommonValue.*;

public class LetterServiceTest extends AppEngineTestCase {

    private LetterService service = new LetterService();

    @Test
    public void test() throws Exception {
        assertThat(service, is(notNullValue()));
    }

    @Test
    public void postLetter(){
        String uid = "setUid";
        String text = "setText";
        String name = "setName";
        UserService.createUserWithUserId(uid);
        LetterService.postLetter(uid, text, name, DEFAULT_STREAM);

        LetterModel model = new LetterModelDao().getLast(1, DEFAULT_STREAM).get(0);

        checkModel(model, UserService.fixUserId(uid), text, name, DEFAULT_STREAM);

        UserModel user = new UserModelDao().getByUserId(UserService.fixUserId(uid));
        assertThat(user.getName(), is(equalTo(name)));
    }

    @Test
    public void postLetterTwice() throws InterruptedException{
        String uid = "setUid";
        String text = "setText";
        String name = "setName";
        String text2 = "setText2";
        String name2 = "setName2";
        UserService.createUserWithUserId(uid);

        LetterService.postLetter(uid, text, name, DEFAULT_STREAM);
        Thread.sleep(100);
        LetterService.postLetter(uid, text2, name2, DEFAULT_STREAM);

        List<LetterModel> list = new LetterModelDao().getLast(2, DEFAULT_STREAM);
        LetterModel model2 = list.get(0);
        LetterModel model = list.get(1);

        checkModel(model, UserService.fixUserId(uid), text, name, DEFAULT_STREAM);

        checkModel(model2, UserService.fixUserId(uid), text2, name2, DEFAULT_STREAM);
    }

    @Test
    public void postLogin(){
        UserModel user = UserService.createUserWithUserId("setUid");
        LetterService.postLogin(user);

        LetterModel model = new LetterModelDao().getLast(1, DEFAULT_STREAM).get(0);

        checkLoginModel(model, DEFAULT_USER_NAME);
    }

    @Test
    public void getLastLetterList() throws InterruptedException{
        UserModel user1 = UserService.createUser("user1",UserService.allocateUserId());
        UserModel user2 = UserService.createUser("user2",UserService.allocateUserId());
        UserModel user3 = UserService.createUser("user3",UserService.allocateUserId());

        LetterService.postLogin(user1);
        Thread.sleep(100);
        LetterService.postLogin(user2);
        Thread.sleep(100);
        LetterService.postLogin(user3);

        List<LetterModel> list = LetterService.getLastLetterList(20, null);

        assertThat(list, is(equalTo(LetterService.getLastLetterList(20, DEFAULT_STREAM))));

        checkLoginModel(list.get(0), user3.getName());
        checkLoginModel(list.get(1), user2.getName());
        checkLoginModel(list.get(2), user1.getName());

        assertThat(list.size(), is(equalTo(3)));
    }

    private void checkModel(LetterModel model, String uid, String text,
            String name, String stream) {
        assertThat(model.getUserId(), is(equalTo(uid)));
        assertThat(model.getWriteData(), is(equalTo(text)));
        assertThat(model.getPostName(), is(equalTo(name)));
        assertThat(model.getStream(), is(equalTo(stream)));
    }

    private void checkLoginModel(LetterModel model, String name) {
        assertThat(model.getUserId(), is(equalTo(SYSTEM_USRE_ID)));
        assertThat(model.getWriteData(), is(containsString(name)));
        assertThat(model.getPostName(), is(equalTo(SYSTEM_USER_NAME)));
        assertThat(model.getStream(), is(equalTo(DEFAULT_STREAM)));
    }
}
