package schat.dao;

import java.util.Date;
import java.util.List;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;

import schat.model.ChannelIdModel;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static schat.CommonValue.*;

public class ChannelIdModelDaoTest extends AppEngineTestCase {

    private ChannelIdModelDao dao = new ChannelIdModelDao();

    @Test
    public void test() throws Exception {
        assertThat(dao, is(notNullValue()));
    }

    @Test
    public void getByStream(){
        ChannelIdModel model = putModel();

        List<ChannelIdModel> stream = dao.getByStream(DEFAULT_STREAM);
        assertThat(stream.get(0), is(equalTo(model)));
    }

    private ChannelIdModel putModel() {
        ChannelIdModel model = new ChannelIdModel();
        model.setChannelId("setChannel");
        model.setCreateTime(new Date().getTime());
        model.setStream(DEFAULT_STREAM);
        dao.put(model);
        return model;
    }

    @Test
    public void getByChannelId(){
        ChannelIdModel model = putModel();

        ChannelIdModel channelId = dao.getByChannelId("setChannel");
        assertThat(channelId, is(model));
    }
}
