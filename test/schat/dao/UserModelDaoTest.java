package schat.dao;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;

import schat.model.UserModel;
import schat.service.UserService;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class UserModelDaoTest extends AppEngineTestCase {

    private UserModelDao dao = new UserModelDao();

    @Test
    public void test() throws Exception {
        assertThat(dao, is(notNullValue()));
    }

    @Test
    public void getByUserId(){
        UserModel model = UserService.createUser("setName", "setId");

        UserModel userId = dao.getByUserId(UserService.fixUserId("setId"));

        assertThat(userId, is(equalTo(model)));
    }
}
