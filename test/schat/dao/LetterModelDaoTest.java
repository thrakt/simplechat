package schat.dao;

import java.util.List;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;

import schat.model.LetterModel;
import schat.model.UserModel;
import schat.service.LetterService;
import schat.service.UserService;
import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.*;
import static org.hamcrest.CoreMatchers.*;
import static schat.CommonValue.*;

public class LetterModelDaoTest extends AppEngineTestCase {

    private LetterModelDao dao = new LetterModelDao();

    @Test
    public void test() throws Exception {
        assertThat(dao, is(notNullValue()));
    }

    @Test
    public void getLast() throws Exception {
        UserModel user1 = UserService.createUser("user1",UserService.allocateUserId());
        UserModel user2 = UserService.createUser("user2",UserService.allocateUserId());
        UserModel user3 = UserService.createUser("user3",UserService.allocateUserId());

        LetterService.postLogin(user1);
        Thread.sleep(100);
        LetterService.postLogin(user2);
        Thread.sleep(100);
        LetterService.postLogin(user3);

        List<LetterModel> list = dao.getLast(10, DEFAULT_STREAM);

        assertThat(list.get(0).getWriteData(), is(containsString(user3.getName())));
        assertThat(list.get(1).getWriteData(), is(containsString(user2.getName())));
        assertThat(list.get(2).getWriteData(), is(containsString(user1.getName())));
        assertThat(list.size(), is(equalTo(3)));
    }
}
