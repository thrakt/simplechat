package schat.model;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class ChannelIdModelTest extends AppEngineTestCase {

    private ChannelIdModel model = new ChannelIdModel();

    @Test
    public void test() throws Exception {
        assertThat(model, is(notNullValue()));
    }
}
