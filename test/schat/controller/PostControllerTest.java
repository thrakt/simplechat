package schat.controller;

import org.slim3.tester.ControllerTestCase;
import org.junit.Test;

import schat.service.UserService;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class PostControllerTest extends ControllerTestCase {

    @Test
    public void run() throws Exception {
        UserService.createUser("sendname", "senduid");
        
        tester.param("uid","senduid");
        tester.param("data","sendtext");
        tester.param("name","sendname");
        tester.start("/post");
        PostController controller = tester.getController();
        assertThat(controller, is(notNullValue()));
        assertThat(tester.isRedirect(), is(false));
        assertThat(tester.getDestinationPath(), is(nullValue()));
    }
}
