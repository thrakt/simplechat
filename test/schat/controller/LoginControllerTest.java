package schat.controller;

import org.slim3.tester.ControllerTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.*;
import static org.hamcrest.CoreMatchers.*;
import static schat.CommonValue.*;

public class LoginControllerTest extends ControllerTestCase {

    @Test
    public void sendName() throws Exception{
        tester.param("user", "sendname");
        tester.start("/login");
        LoginController controller = tester.getController();
        assertThat(controller, is(notNullValue()));
        assertThat(tester.isRedirect(), is(false));
        assertThat(tester.getDestinationPath(), is(nullValue()));
        assertThat(tester.response.getOutputAsString(),is(containsString("\"name\":\"sendname\"")));
        assertThat(tester.response.getOutputAsString(),is(not(containsString("\"uid\":\"\""))));
    }
    
    @Test
    public void sendUid() throws Exception{
        tester.param("user", "#senduid");
        tester.start("/login");
        LoginController controller = tester.getController();
        assertThat(controller, is(notNullValue()));
        assertThat(tester.isRedirect(), is(false));
        assertThat(tester.getDestinationPath(), is(nullValue()));
        assertThat(tester.response.getOutputAsString(),is(containsString("\"name\":\""+DEFAULT_USER_NAME+"\"")));
        assertThat(tester.response.getOutputAsString(),is(not(containsString("'uid':'sendid'"))));
    }
    
    @Test
    public void loginAgain() throws Exception{
        tester.param("user", "sendname");
        tester.start("/login");
        LoginController controller = tester.getController();
        assertThat(controller, is(notNullValue()));
        assertThat(tester.isRedirect(), is(false));
        assertThat(tester.getDestinationPath(), is(nullValue()));
        String outStr = tester.response.getOutputAsString();
        assertThat(outStr,is(containsString("\"name\":\"sendname\"")));
        assertThat(outStr,is(not(containsString("\"uid\":\"\""))));
        
        int start = outStr.indexOf("\"uid\":")+7;
        String uid = outStr.substring(start,outStr.indexOf("\"", start));
        
        tester.param("user","#"+uid);
        tester.start("/login");
        controller = tester.getController();
        assertThat(controller, is(notNullValue()));
        assertThat(tester.isRedirect(), is(false));
        assertThat(tester.getDestinationPath(), is(nullValue()));
        assertThat(tester.response.getOutputAsString(),is(containsString("\"name\":\"sendname\"")));
        assertThat(tester.response.getOutputAsString(),is(containsString("\"uid\":\""+uid+"\"")));
        
        
    }
}
