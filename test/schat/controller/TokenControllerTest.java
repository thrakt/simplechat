package schat.controller;

import org.slim3.tester.ControllerTestCase;
import org.junit.Test;

import schat.dao.ChannelIdModelDao;
import schat.model.ChannelIdModel;
import schat.service.UserService;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static schat.CommonValue.*;

public class TokenControllerTest extends ControllerTestCase {

    @Test
    public void run() throws Exception {
        tester.param("uid","senduid");
        tester.start("/token");
        TokenController controller = tester.getController();
        assertThat(controller, is(notNullValue()));
        assertThat(tester.isRedirect(), is(false));
        assertThat(tester.getDestinationPath(), is(nullValue()));
        assertThat(tester.response.getOutputAsString(), is(notNullValue()));
        
        ChannelIdModel model = new ChannelIdModelDao().getByChannelId(UserService.fixUserId("senduid"));
        assertThat(model, is(notNullValue()));
        assertThat(model.getStream(), is(equalTo(DEFAULT_STREAM)));
    }
}
