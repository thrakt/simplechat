package schat.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import schat.dao.UserModelDao;
import schat.model.UserModel;

import static schat.CommonValue.*;

/**
 * Service to create/get {@link UserModel}
 */
public class UserService {

    private static final String SOLT = "KR8K1O6KQN31ANWVHI49IW6E3QEN9GB7";

    /**
     * Getting User by User Fixed Id
     *
     * @param userId fixed user id {@link #fixUserId(String)}
     * @return
     */
    public static UserModel getUserById(String userId){
        return new UserModelDao().getByUserId(userId);
    }

    /**
     * Fix User Id was inputed for datastore
     *
     * @param userId inputed user id
     * @return fixed
     */
    public static String fixUserId(String userId) {
        // processed for secure
        userId = userId + SOLT + userId;

        String result = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(userId.getBytes());

            StringBuilder buf = new StringBuilder();
            for (byte b : digest.digest()) {
                buf.append(Integer.toHexString((b >> 4) & 0x0F));
                buf.append(Integer.toHexString(b & 0x0F));
            }
            result = buf.toString();
        } catch (NoSuchAlgorithmException e) {
            result = "";
        }

        return result;
    }

    /**
     * create user specify user id with default name
     *
     * @param userId
     * @return
     */
    public static UserModel createUserWithUserId(String userId) {
        return createUser(DEFAULT_USER_NAME, userId);
    }

    /**
     * create user
     *
     * @param name
     * @param id raw User Id
     * @return
     */
    public static UserModel createUser(String name,String id){
        UserModel model = new UserModel();
        model.setName(name);
        model.setUserId(fixUserId(id));
        new UserModelDao().putAsync(model);
        return model;
    }

    /**
     * get raw User Id
     *
     * @return
     */
    public static String allocateUserId() {
        return fixUserId(String.valueOf(new Random().nextInt())).substring(0, 8);
    }

    /**
     * update user name
     *
     * @param userId raw User Id
     * @param postName update name
     */
    public static void updateName(String userId, String postName) {
        UserModelDao dao = new UserModelDao();
        UserModel model = dao.getByUserId(fixUserId(userId));
        model.setName(postName);
        // not need sync
        dao.putAsync(model);
    }

}
