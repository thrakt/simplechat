package schat.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.appengine.api.channel.ChannelFailureException;
import com.google.appengine.api.channel.ChannelMessage;
import com.google.appengine.api.channel.ChannelService;
import com.google.appengine.api.channel.ChannelServiceFactory;

import schat.JsonUtil;
import schat.dao.ChannelIdModelDao;
import schat.model.ChannelIdModel;
import schat.model.LetterModel;

import static schat.CommonValue.*;

/**
 * Service to notify
 */
public class NotificationService {

    private static ChannelService channel = ChannelServiceFactory.getChannelService();

    /**
     * Notify letter for user
     *
     * @param model
     */
    public static void notifyPost(LetterModel model) {
        for(String s: getClients(model.getStream())){
            ChannelMessage cm = new ChannelMessage(s, JsonUtil.getLetterJson(model));
            try {
                channel.sendMessage(cm);
            } catch (ChannelFailureException e) {
                e.printStackTrace();
            }
        }
    }

    private static List<String> getClients(String stream) {
        List<String> result = new ArrayList<String>();

        long expTime = new Date().getTime() - 1000*60*60*2 - 1000*60*5;

        for(ChannelIdModel id: new ChannelIdModelDao().getByStream(stream)){
            if(id.getCreateTime() > expTime){
                result.add(id.getChannelId());
            } else {
                // deleted when expired
                // (Expiration date of the appengine channel id is 2 hours)
                new ChannelIdModelDao().deleteAsync(id.getKey());
            }
        }

        return result;
    }

    /**
     * create token for channel
     *
     * @param userId raw User Id
     * @return
     */
    public static String createToken(String userId) {
        String fixUserId = UserService.fixUserId(userId);
        ChannelIdModel exist = new ChannelIdModelDao().getByChannelId(fixUserId);

        // delete when already have
        if(exist != null){
            new ChannelIdModelDao().delete(exist.getKey());
        }

        ChannelIdModel model = new ChannelIdModel();
        model.setChannelId(fixUserId);
        model.setStream(DEFAULT_STREAM);
        model.setCreateTime(new Date().getTime());
        new ChannelIdModelDao().put(model);

        return channel.createChannel(fixUserId);
    }

}
