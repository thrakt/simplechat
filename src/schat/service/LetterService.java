package schat.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slim3.datastore.ModelRef;

import schat.dao.LetterModelDao;
import schat.model.IconModel;
import schat.model.LetterModel;
import schat.model.UserModel;

import static schat.CommonValue.*;

/**
 * Service to post/get {@link LetterModel}
 *
 */
public class LetterService {

    /**
     * Letter post to Datastore and Notify to user
     *
     * @param userId raw User Id
     * @param writeData
     * @param postName
     * @param stream if null then set default
     */
    public static void postLetter(String userId, String writeData,
            String postName, String stream) {

        LetterModel model = new LetterModel();
        model.setUserId(fixUserId(userId));
        model.setWriteData(fixWriteData(writeData));
        model.setPostName(fixPostName(postName));
        model.setStream(fixStream(stream));
        model.setPostDate(getCurrentTime());

        UserModel user = UserService.getUserById(fixUserId(userId));

        ModelRef<IconModel> icon = user.getIcon();
        if(icon != null){
            model.getIcon().setKey(icon.getKey());
        }

        // if input new name, then save to user data
        if(user.getName().equals(postName) == false){
            UserService.updateName(userId,postName);
        }

        new LetterModelDao().put(model);

        NotificationService.notifyPost(model);
    }

    private static String fixUserId(String userId) {
        return UserService.fixUserId(userId);
    }

    private static String fixStream(String stream) {
        return stream == null || stream.equals("") ? DEFAULT_STREAM : stream;
    }

    private static String fixPostName(String postName) {
        return postName == null || postName.equals("")
            ? DEFAULT_USER_NAME
            : postName;
    }

    private static String  fixWriteData(String writeData) {
        if(writeData == null || writeData.equals("")){
            writeData = " ";
        }
        return writeData.replaceAll("\\r\\n|\\r|\\n", " ");
    }

    private static Date getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        // edit on japan time (GMT+9) for viewing
        cal.add(Calendar.HOUR_OF_DAY, 9);
        return cal.getTime();
    }

    /**
     * Letter post to Database at Login user
     *
     * @param user
     */
    public static void postLogin(UserModel user) {

        // post by SYSTEM user
        LetterModel model = new LetterModel();
        model.setUserId(SYSTEM_USRE_ID);
        model.setWriteData(fixWriteData(LOGIN_MESSAGE.replace("$USER",user.getName())));
        model.setPostName(SYSTEM_USER_NAME);
        model.setStream(DEFAULT_STREAM);
        model.setPostDate(getCurrentTime());

        new LetterModelDao().put(model);
        NotificationService.notifyPost(model);
    }

    /**
     * get Last Letters
     *
     * @param requestNumber
     * @param stream
     * @return order by date descending
     */
    public static List<LetterModel> getLastLetterList(int requestNumber,String stream){
        return new LetterModelDao().getLast(requestNumber,
                                              stream == null ? DEFAULT_STREAM : stream);
    }
}
