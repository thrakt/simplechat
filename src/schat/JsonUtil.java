package schat;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;

import org.slim3.datastore.ModelRef;

import com.google.appengine.api.datastore.KeyFactory;

import schat.model.IconModel;
import schat.model.LetterModel;
import schat.model.UserModel;

/**
 * Utility to convert JSON
 *
 */
public class JsonUtil {

    private static final String LETTER_FORMAT =
        "'name':'{0}','date':'{1}','data':'{2}','uid':'{3}','icon':'{4}','stream':'{5}'";

    private static final String USER_FORMAT = "'name':'{0}','uid':'{1}','icon':'{2}'";

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    /**
     * get JSON of Letter
     *
     * @param model convet to JSON
     * @return
     */
    public static String getLetterJson(LetterModel model) {
        return encloseBrackets(MessageFormat.format(
            LETTER_FORMAT.replace("'", "\""),
            new Object[] {
                model.getPostName(),
                DATE_FORMAT.format(model.getPostDate()),
                model.getWriteData(),
                model.getUserId(),
                getIconString(model.getIcon()),
                model.getStream() }));
    }

    private static String getIconString(ModelRef<IconModel> modelRef) {
        return modelRef != null && modelRef.getKey() != null
            ? KeyFactory.keyToString(modelRef.getKey())
            : "";
    }

    /**
     * get JSON of user
     *
     * @param model convet to JSON
     * @param uid raw User Id
     * @return
     */
    public static String getUserJson(UserModel model, String uid) {
        return encloseBrackets(MessageFormat
            .format(
                USER_FORMAT.replace("'", "\""),
                new Object[] {
                    model.getName(),
                    uid,
                    getIconString(model.getIcon()) }));
    }

    private static String encloseBrackets(String str) {
        return "{" + str + "}";
    }



}
