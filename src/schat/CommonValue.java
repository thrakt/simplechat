package schat;

/**
 * Provide constant values static
 *
 */
public class CommonValue {
    public static final String DEFAULT_USER_NAME = "NamelessOne";
    public static final String DEFAULT_STREAM = "global";
    public static final String SYSTEM_USRE_ID = "ADMIN";
    public static final String SYSTEM_USER_NAME = "SYSYTEM MESSAGE";
    public static final String LOGIN_MESSAGE = "$USER entry.";
}