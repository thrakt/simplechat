package schat.dao;

import java.util.List;

import org.slim3.datastore.DaoBase;
import org.slim3.datastore.Datastore;

import schat.meta.ChannelIdModelMeta;
import schat.model.ChannelIdModel;

public class ChannelIdModelDao extends DaoBase<ChannelIdModel>{

    private static final ChannelIdModelMeta meta = ChannelIdModelMeta.get();

    public List<ChannelIdModel> getByStream(String stream) {
        return Datastore.query(meta).filter(meta.stream.equal(stream)).asList();
    }

    public ChannelIdModel getByChannelId(String userId) {
        return Datastore.query(meta).filter(meta.channelId.equal(userId)).asSingle();
    }

}
