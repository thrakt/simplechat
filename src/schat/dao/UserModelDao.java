package schat.dao;

import org.slim3.datastore.DaoBase;
import org.slim3.datastore.Datastore;

import schat.meta.UserModelMeta;
import schat.model.UserModel;

public class UserModelDao extends DaoBase<UserModel> {

    private static final UserModelMeta meta = UserModelMeta.get();

    public UserModel getByUserId(String userId) {
        return Datastore
            .query(meta)
            .filter(meta.userId.equal(userId))
            .asSingle();
    }

}
