package schat.dao;

import java.util.List;

import org.slim3.datastore.DaoBase;
import org.slim3.datastore.Datastore;

import schat.meta.LetterModelMeta;
import schat.model.LetterModel;

public class LetterModelDao extends DaoBase<LetterModel>{
    
    private static final LetterModelMeta meta = LetterModelMeta.get();

    public List<LetterModel> getLast(int requestNumber, String stream) {
        return Datastore
            .query(meta)
            .filter(meta.stream.equal(stream))
            .sort(meta.postDate.desc)
            .limit(requestNumber)
            .asList();
    }

}
