package schat.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import schat.service.NotificationService;

public class TokenController extends Controller {

    @Override
    public Navigation run() throws Exception {
        String userId = (String) request.getAttribute("uid");
        String token = NotificationService.createToken(userId);
        
        response.setContentType("text/plain; charset=utf-8");
        response.getWriter().write(token);
        
        return null;
    }
}
