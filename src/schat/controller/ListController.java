package schat.controller;

import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import schat.JsonUtil;
import schat.model.LetterModel;
import schat.service.LetterService;

public class ListController extends Controller {

    @Override
    public Navigation run() throws Exception {
        String numStr = (String) request.getAttribute("num");
        int num = numStr == null ? 20 : Integer.parseInt(numStr);
        String stream = (String) request.getAttribute("stream");

        List<LetterModel> list = LetterService.getLastLetterList(num, stream);

        // make JSON array for reply Letter list
        StringBuilder buf = new StringBuilder();
        buf.append("[");
        for(int i=0;i<list.size();){
            buf.append(JsonUtil.getLetterJson(list.get(i)));
            if(++i<list.size()) buf.append(",");
        }
        buf.append("]");

        response.setContentType("text/plain; charset=utf-8");
        response.getWriter().write(buf.toString());
        return null;
    }
}
