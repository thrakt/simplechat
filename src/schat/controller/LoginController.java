package schat.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import schat.JsonUtil;
import schat.model.UserModel;
import schat.service.LetterService;
import schat.service.UserService;

public class LoginController extends Controller {

    @Override
    public Navigation run() throws Exception {
        String input = (String) request.getAttribute("user");
        String uid;
        UserModel user;

        // if input #CODE ,then specify User Id, else generate id
        if(input != null && input.startsWith("#")){
            uid = input.substring(1);
            user = loginWithUid(uid);
        } else {
            uid = UserService.allocateUserId();
            user = loginNew(input, uid);
        }

        // notify login
        LetterService.postLogin(user);

        response.setContentType("text/plain; charset=utf-8");
        response.getWriter().write(JsonUtil.getUserJson(user,uid));
        return null;
    }

    private UserModel loginWithUid(String uid) {
        UserModel user = UserService.getUserById(UserService.fixUserId(uid));
        if(user == null){
            user = UserService.createUserWithUserId(uid);
        }
        return user;

    }

    private UserModel loginNew(String name, String uid) {
        return UserService.createUser(name, uid);
    }
}
