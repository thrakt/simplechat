package schat.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import schat.service.LetterService;

public class PostController extends Controller {

    @Override
    public Navigation run() throws Exception {
        postingLetter();
        return null;
    }

    private boolean postingLetter() {
        String userId = (String) request.getAttribute("uid");
        if (userId == null || userId.equals("")) {
            return false;
        }
        String writeData = (String) request.getAttribute("data");
        String postName = (String) request.getAttribute("name");
        String stream = (String) request.getAttribute("stream");
        
        LetterService.postLetter(userId, writeData, postName, stream);
        
        return true;
        
    }
}
